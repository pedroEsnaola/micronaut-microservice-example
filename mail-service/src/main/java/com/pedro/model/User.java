package com.pedro.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {

    Long id;

    @NotEmpty
    String username;

    @Email
    String email;

    @NotEmpty
    String password;
}
