package com.pedro.service;

import com.pedro.model.User;

public interface TemplateService {

    String generateUserSignInTemplate(User user);
}
