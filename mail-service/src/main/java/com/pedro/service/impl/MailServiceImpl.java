package com.pedro.service.impl;

import com.pedro.service.MailService;
import com.pedro.service.TemplateService;
import lombok.extern.slf4j.Slf4j;
import org.simplejavamail.api.email.Email;
import org.simplejavamail.api.mailer.AsyncResponse;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.email.EmailBuilder;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Slf4j
public class MailServiceImpl  implements MailService {

    @Inject
    Mailer mailer;

    @Inject
    TemplateService s;

    @Override
    public void sendEmail(String to, String subject, String text) {
        Email email = EmailBuilder.startingBlank()
                .from("pedro.esnaola23@gmail.com")
                .to(to)
                .withSubject(subject)
                .withHTMLText(text).buildEmail();
        AsyncResponse asyncResponse = mailer.sendMail(email, true);
        asyncResponse.onSuccess(() -> log.info("Email sended to "+to));
        asyncResponse.onException((e) -> log.error("Error sending email to" + to, e));
    }
}
