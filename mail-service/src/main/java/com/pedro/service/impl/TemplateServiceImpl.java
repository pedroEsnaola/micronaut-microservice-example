package com.pedro.service.impl;

import com.pedro.model.User;
import com.pedro.service.TemplateService;
import io.micronaut.context.annotation.Prototype;
import io.micronaut.views.thymeleaf.ThymeleafViewsRenderer;
import lombok.SneakyThrows;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.IContext;

import javax.inject.Inject;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@Prototype
public class TemplateServiceImpl implements TemplateService {

    @Inject
    ThymeleafViewsRenderer renderer;

    @SneakyThrows
    @Override
    public String generateUserSignInTemplate(User u) {
        ByteArrayOutputStream o = new ByteArrayOutputStream();
        renderer.render("UserSignIn", u).writeTo(o);
        return o.toString(StandardCharsets.UTF_8);
    }
}
