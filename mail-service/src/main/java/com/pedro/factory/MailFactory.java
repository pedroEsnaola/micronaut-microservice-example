package com.pedro.factory;

import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.ConfigurationProperties;
import io.micronaut.context.annotation.Factory;
import lombok.extern.slf4j.Slf4j;
import org.simplejavamail.api.mailer.Mailer;
import org.simplejavamail.api.mailer.config.TransportStrategy;
import org.simplejavamail.mailer.MailerBuilder;

import javax.inject.Singleton;

@ConfigurationProperties("mail.sender")
@Factory
@Slf4j
public class MailFactory {

    String host;
    int port;
    String username;
    String password;


    @Singleton
    public Mailer mailer(){
            return MailerBuilder.withSMTPServer(host, port)
                .withSMTPServerUsername(username)
                .async()
                .withSMTPServerPassword(password)
                .withTransportStrategy(TransportStrategy.SMTP).clearEmailAddressCriteria()
                .buildMailer();
    }
}
