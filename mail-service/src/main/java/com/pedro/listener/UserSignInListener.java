package com.pedro.listener;

import com.pedro.model.User;
import com.pedro.service.MailService;
import com.pedro.service.TemplateService;
import io.micronaut.configuration.rabbitmq.annotation.Queue;
import io.micronaut.configuration.rabbitmq.annotation.RabbitListener;

import javax.inject.Inject;

@RabbitListener
public class UserSignInListener {

    @Inject
    MailService mailService;

    @Inject
    TemplateService templateService;

    @Queue("users")
    public void receive(User user){
        mailService.sendEmail(user.getEmail(), "Welcome", templateService.generateUserSignInTemplate(user));
    }

}
