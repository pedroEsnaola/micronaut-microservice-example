package api.controller;

import api.model.CustomUserDetails;
import api.model.Todo;
import api.service.TodoService;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.token.jwt.validator.AuthenticationJWTClaimsSetAdapter;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.reactivestreams.Publisher;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.security.Principal;
import java.util.TooManyListenersException;

@Controller("/todo")
@Secured("isAuthenticated()")
public class TodoController {

    @Inject
    TodoService service;

    @Post
    @Status(HttpStatus.CREATED)
    public Single<Todo> create(@Body Todo todo, Principal principal){
        return service.save(todo, principal);
    }

    @Get
    @Status(HttpStatus.OK)
    public Publisher<Todo> findAll(Principal principal){
        return service.findByUserId((Long)((AuthenticationJWTClaimsSetAdapter) principal).getAttributes().get("user_id"));
    }

    @Post("/{id}")
    @Status(HttpStatus.OK)
    public Single<Todo> complete(@PathVariable String id, Principal principal){
        return service.complete(id, principal);
    }

    @Delete("/{id}")
    @Status(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id, Principal principal){
        service.deleteTodo(id, principal);
    }
}
