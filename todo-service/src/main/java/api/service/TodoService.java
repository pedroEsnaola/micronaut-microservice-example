package api.service;


import api.model.Todo;
import io.reactivex.Flowable;
import io.reactivex.Single;
import org.reactivestreams.Publisher;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.security.Principal;

public interface TodoService {

    Single<Todo> save(@Valid Todo todo, Principal principal);

    Publisher<Todo> findByUserId(@NotNull Long userId);

    Single<Todo> complete(@NotNull String todoId, Principal principal);

    void deleteTodo(@NotNull String todoId, Principal principal);

}
