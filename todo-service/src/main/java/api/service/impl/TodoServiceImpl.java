package api.service.impl;


import api.model.CustomUserDetails;
import api.model.Todo;
import api.service.TodoService;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;
import io.micronaut.security.authentication.UserDetails;
import io.micronaut.security.token.jwt.validator.AuthenticationJWTClaimsSetAdapter;
import io.reactivex.Flowable;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.bson.BsonDocument;
import org.bson.BsonDocumentWriter;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.print.Doc;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.awt.desktop.QuitEvent;
import java.security.Principal;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.Flow;

@Singleton
@Slf4j
public class TodoServiceImpl implements TodoService {

    @Inject
    MongoClient client;

    @Override
    public Single<Todo> save(@Valid Todo todo, Principal principal) {
        return Single.defer(() -> {

            todo.setUserId((Long)((AuthenticationJWTClaimsSetAdapter) principal).getAttributes().get("user_id"));
            todo.setCreatedAt(new Date());
            todo.setDone(false);
            Single.fromPublisher(getCollection().insertOne(todo)).subscribe();
            return Single.just(todo);
        });
    }

    @Override
    public Flowable<Todo> findByUserId(@NotNull Long userId) {
        Document d = new Document();
        d.append("userId", userId);
        return Flowable.fromPublisher(getCollection().find(d)).map(todo -> {
            log.info(todo.toString());
            return todo;
        });
    }

    @Override
    public Single<Todo> complete(@NotNull String todoId, Principal principal) {
//        Bug no update ainda esperando correção pelo time do mongo https://jira.mongodb.org/browse/JAVA-2114
//        Document query = new Document();
//        query.append("_id", todoId);
//        query.append("userId", (Long)((AuthenticationJWTClaimsSetAdapter) principal).getAttributes().get("user_id"));
//        Document update = new Document();
//        update.append("done", true);
//        return Single.fromPublisher(getCollection().findOneAndUpdate(query,update));
        Document query = new Document();
        query.append("_id", new ObjectId(todoId));
        return Single.fromPublisher(getCollection().find(query).first()).toMaybe()
                .switchIfEmpty(Single.error(RuntimeException::new))
                .flatMap(t -> {

                    if(t.getUserId().equals((Long)((AuthenticationJWTClaimsSetAdapter) principal).getAttributes().get("user_id"))){
                        t.setDone(true);
                        getCollection().insertOne(t);
                    }else{
                        Single.error(RuntimeException::new);
                    }
                    return Single.just(t);
                });
    }

    @Override
    public void deleteTodo(@NotNull String todoId, Principal principal) {
        Document query = new Document();
        query.append("_id", new ObjectId(todoId));
        Single.fromPublisher(getCollection().deleteOne(query)).subscribe();
    }

    private MongoCollection<Todo> getCollection(){
        return client.getDatabase("todo-app").getCollection("todos", Todo.class);
    }

}
