package api.model;

import io.micronaut.security.authentication.UserDetails;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class CustomUserDetails extends UserDetails {

    @Getter
    Long userId;

    public CustomUserDetails(String username, Collection<String> roles) {
        super(username, roles);
    }

    public CustomUserDetails(String username, Collection<String> roles, Map<String, Object> attributes) {
        super(username, roles, attributes);
    }

    public CustomUserDetails(String username, Long userId){
        super(username, new ArrayList<>(), new HashMap<>());
        this.userId = userId;
    }
}
