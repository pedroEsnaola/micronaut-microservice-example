package api.model;

import io.micronaut.core.annotation.Introspected;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Introspected
public class Todo {

    @BsonId
    @BsonProperty("_id")
    ObjectId id;

    @NotEmpty
    String todo;

    Date createdAt;

    boolean done;

    Long userId;
}
